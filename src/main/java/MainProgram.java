import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MainProgram {

	public static void main(String[] args) throws IOException {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("players");

		List<Player> list = Arrays.asList(new Player("Dhoni", "CSK"), new Player("Rohit", "MI"),
				new Player("Virat", "RCB"), new Player("Raina", "CSK")

		);

		// savePlayer(sheet, list);

		//
//		FileOutputStream fileOutputStream = new FileOutputStream("data.xlsx");
//
//		workbook.write(fileOutputStream);
//		workbook.close();
//		System.out.println("Saved");

		readData();
	}

	private static void readData() throws IOException {

		XSSFWorkbook workbook = new XSSFWorkbook("data.xlsx");
		XSSFSheet sheet = workbook.getSheet("players");
		int count = sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < count; i++) {
			XSSFRow row = sheet.getRow(i);
			System.err.print(row.getCell(0).getStringCellValue());
			System.err.println("," + row.getCell(1).getStringCellValue());

		}

	}

	private static void savePlayer(XSSFSheet sheet, List<Player> list) {

		XSSFRow row = sheet.createRow(0);
		row.createCell(0).setCellValue("NAME");
		row.createCell(1).setCellValue("TEAM");

		for (int i = 1; i <= list.size(); i++) {
			row = sheet.createRow(i);
			row.createCell(0).setCellValue(list.get(i - 1).getName());
			row.createCell(1).setCellValue(list.get(i - 1).getTeam());

		}

	}
}
